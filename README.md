# MealRecipe
>This app aims to provide users with meal recipes from online database. 
>This app allows user to log in or sign up with Firebase auth.
>The first tab will show us latest meals in the web database, and also allow users to search for what they wanna see.
>Second tab displays users favorite meals which can be tagged through recipe detail page.
>Third tab allows users to post some words and pic, which can be anything they want, and display it to everyone who uses this app.
>In our app, we use a library named Retrofit, which is extremlly simplify the process of fetching data from web service.
>In traditinal way, we need to do some asyc task and viewmodel things, but with retrofit, we simplify hundreds lines of code to just a few.
>And in order to diplay video within our app, we implement youtubebaseactivity, which allows video to be displayed within app UI.
